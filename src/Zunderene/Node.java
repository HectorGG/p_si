package Zunderene;

import ontology.Types;
import ontology.Types.ACTIONS;
import tools.Direction;
import tools.Vector2d;

public class Node implements Comparable<Node> {
	
	public double coste_totales,coste_estimado, coste_castigo; 
	public Node progenitor;
	public Vector2d p_actual;
	public Direction action; 
	public int profundidad = 0;
	
	public Node(Vector2d pos) {
		this.coste_totales = 1.0f;
		this.coste_estimado = 0.0f;
		this.p_actual = pos;
		this.action = Types.DNIL;
		
	}
	
	
	/** Mecanismo de ordenacion, estable la lista de prioridad **/
	@Override
	public int compareTo(Node n) {
		  if((this.coste_estimado + this.coste_totales )  < (n.coste_estimado + n.coste_totales ))
	            return -1;
          if((this.coste_estimado + this.coste_totales )  > (n.coste_estimado + n.coste_totales))
	            return 1;
	        return 0;
	}
	
	public boolean equals(Object o) {
		return this.p_actual.equals(((Node)o).p_actual);
	}
	
	/** Determina el siguiente movimiento **/
	public Direction setMoveDir(Node pre) {
		Direction ac = Types.DNONE;

        if(pre.p_actual.x > this.p_actual.x)
        	ac = Types.DRIGHT;
        if(pre.p_actual.x < this.p_actual.x)
        	ac = Types.DLEFT;

        if(pre.p_actual.y > this.p_actual.y)
        	ac = Types.DDOWN;
        if(pre.p_actual.y < this.p_actual.y)
        	ac = Types.DUP;
        
        return ac;

	}
	
	public String toString() {
		return this.p_actual + " ";
	}

}
