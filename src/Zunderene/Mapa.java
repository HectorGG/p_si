package Zunderene;

import java.util.ArrayList;
import java.util.Arrays;

import Zunderene_02.frame;
import core.game.Observation;
import core.game.StateObservation;
import ontology.Types;

import tools.Vector2d;


/**         SISTEMA DE MAPEO 
 * 	Almacena la situacion del mapa.
 *  Indica el lugar de los obstaculos.
 *  
 * 
 * 
 * 
 * **/


public class Mapa {
	
	public ArrayList<Integer> obstaculos;
	public ArrayList<Observation> celda[][];
	public double costArt[][];
	public ArrayList<Observation>[] vehiculos;
	public StateObservation observacion;
	public Node my,objective;
	public boolean noSol = false;
	
	private Start astart;
	
	private int[] x_arrNeig = new int[]{0, 0,    0,    -1,    1};
    private int[] y_arrNeig = new int[]{0, -1,   1,     0,    0};
	
	public Mapa(ArrayList<Integer> obstaculos2) {
		this.obstaculos = obstaculos2;
		this.astart = new Start();
		//this.fr = new frame(observacion.getWorldDimension().width, observacion.getWorldDimension().height);
		
	}
	
	
	public Node run(StateObservation observacion) {
		this.observacion = observacion;
		this.celda = observacion.getObservationGrid();
		
		
		if(observacion.getPortalsPositions() != null) {
			
			this.objective = new Node(observacion.getPortalsPositions()[0].get(0).position);
			this.my = new Node(observacion.getAvatarPosition());
		
			
			my.p_actual.x = (int) my.p_actual.x / observacion.getBlockSize();
			my.p_actual.y = (int) my.p_actual.y / observacion.getBlockSize();
			
			objective.p_actual.x = (int)objective.p_actual.x  / observacion.getBlockSize();
			objective.p_actual.y = (int)objective.p_actual.y / observacion.getBlockSize();
			
			vehiculos = observacion.getMovablePositions();
			costArt = new double[celda.length][celda[0].length];
			
			for(int i = 0; i < costArt.length; i++) {
				for(int j = 0; j < costArt[i].length; j++) {
					costArt[i][j] = 1;
				}
			}
			
			updateCelda();
			
			Node move = this.astart.run(my, objective, this);
			
		
			return move;
		}
		
		else if(observacion.getMovablePositions() != null){
			
			//this.objective = new Node(observacion.getPortalsPositions()[0].get(0).position);
			this.my = new Node(observacion.getAvatarPosition());
		
			
			my.p_actual.x = (int) my.p_actual.x / observacion.getBlockSize();
			my.p_actual.y = (int) my.p_actual.y / observacion.getBlockSize();
			
			// objective.p_actual.x = (int)objective.p_actual.x  / observacion.getBlockSize();
			// objective.p_actual.y = (int)objective.p_actual.y / observacion.getBlockSize();
			
			this.objective = new Node(new Vector2d(13, 1));
			
			vehiculos = observacion.getMovablePositions();
			costArt = new double[celda.length][celda[0].length];
			for(int i = 0; i < costArt.length; i++) {
				for(int j = 0; j < costArt[i].length; j++) {
					costArt[i][j] = 1;
				}
			}
			updateCelda();
			
			Node move = this.astart.run(my, objective, this);
			
		
			return move;
		}

		else {
			return null;
		}
	}
	
private void updateCelda() {
		
		for (int i = 0; i < vehiculos.length; i++) {
			
			for(int indeType = 0; indeType < vehiculos[i].size(); indeType++) {
				int posX = (int) (vehiculos[i].get(indeType).position.x / observacion.getBlockSize());
				int posY = (int) (vehiculos[i].get(indeType).position.y / observacion.getBlockSize());
				
				switch (vehiculos[i].get(indeType).itype) {
				case 8:
					if(posX + 2 < costArt.length) costArt[posX + 2][posY] = 80000;
					if(posX + 3 < costArt.length) costArt[posX + 3][posY] = 80000;
					if(posX + 1 < celda.length) celda[posX + 1][posY].add(vehiculos[i].get(indeType));
					break;
				case 11:
					if(posX - 2 > 0) costArt[posX - 2][posY] = 80000;
					if(posX - 3 > 0) costArt[posX - 3][posY] = 80000;
					if(posX - 1 > 0) celda[posX - 1][posY].add(vehiculos[i].get(indeType)); 
					break;
				case 7:
					if(posX + 1 < celda.length) celda[posX + 1][posY].add(vehiculos[i].get(indeType));
					
					if(posX + 1 < costArt.length) costArt[posX + 1][posY] = 990000;
					if(posX + 2 < costArt.length) costArt[posX + 2][posY] = 990000;
					if(posX + 3 < costArt.length) costArt[posX + 3][posY] = 90000;
					if(posX + 4 < costArt.length) costArt[posX + 4][posY] = 1;
					//if(posX + 3 < celda.length) celda[posX + 3][posY].add(vehiculos[i].get(indeType));
					break;
				case 10:
					if(posX - 1 > 0) celda[posX - 1][posY].add(vehiculos[i].get(indeType));
					if(posX - 1 > 0) costArt[posX - 1][posY] = 990000;
					if(posX - 2 > 0) costArt[posX - 2][posY] = 90000;
					if(posX - 3 > 0) costArt[posX - 3][posY] = 80000;
					if(posX - 4 > 0) costArt[posX - 4][posY] = 1;
					//if(posX - 3 > 0) celda[posX - 3][posY].add(vehiculos[i].get(indeType));
					break;
				default:
					break;
				}
			}
		}
	}
	
    private boolean isObstacle(Vector2d newVector){
        if(newVector.x < 0 || newVector.x >= celda.length) return true;
        if(newVector.y < 0 || newVector.y >= celda[0].length) return true;
        
        for(Observation obs : celda[(int) newVector.x ][(int) newVector.y])
        {
            if(obstaculos.contains(obs.category)) {
            	return true;
            }
        }
        
        //if(!celda[(int) newVector.x ][(int) newVector.y].isEmpty()) System.out.println(celda[(int) newVector.x ][(int) newVector.y]);;
        
        return false;
        

    }
    
    public ArrayList<Node> GeneradorDescendiente(Node node, Node destino) {
        ArrayList<Node> hijos = new ArrayList<Node>();
        int x = (int) node.p_actual.x; 
        int y = (int) node.p_actual.y; 

        for(int i = 0; i < x_arrNeig.length; ++i){
        	Vector2d newVector = new Vector2d( x_arrNeig[i] + x,  y_arrNeig[i] + y);
            if(!isObstacle(newVector)){
            	Node hijo = new Node(newVector);
            	hijo.action = node.setMoveDir(hijo);
            	hijo.coste_castigo = costArt[ x_arrNeig[i] + x][ y_arrNeig[i] + y];
            	hijo.profundidad = 0;
            	hijos.add(hijo);
            }
        }

        return hijos;
    }
    
    public static double heuristicEstimatedCost(Node curNode, Node goalNode){
        
        double xDiff =  Math.pow(Math.abs(curNode.p_actual.x - goalNode.p_actual.x),2);
        double yDiff =  Math.pow(Math.abs(curNode.p_actual.y - goalNode.p_actual.y),2);
        return Math.sqrt(xDiff + yDiff);
    }

}


