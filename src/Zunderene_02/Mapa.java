package Zunderene_02;

import java.util.ArrayList;
import java.util.Arrays;

import Zunderene_02.Node;
import Zunderene_02.Arbol.pregunta;
import Zunderene_02.Arbol.Accion;
import Zunderene_02.Arbol;
import core.game.Observation;
import core.game.StateObservation;
import ontology.Types;

import tools.Vector2d;


/**         SISTEMA DE MAPEO 
 * 	Almacena la situacion del mapa.
 *  Indica el lugar de los obstaculos.
 *  
 * 
 * 
 * 
 * **/


public class Mapa {
	
	private int[] x_arrNeig = new int[]{ 0,    0,    -1,    1};
    private int[] y_arrNeig = new int[]{ -1,   1,     0,    0};
    
    private StateObservation observacion;
    
    public ArrayList<Observation> [][] celda;
	public ArrayList<Observation> [] Obs_inamobibles;					
	public ArrayList<Observation>  ENEMIGOS   = new ArrayList<Observation>();						
	public ArrayList<Observation>  OBJETIVOS  = new ArrayList<Observation>();	
	public ArrayList<Observation>  GUSANOS    = new ArrayList<Observation>();	
	public int [][] coste;
	public ArrayList<Node>  Objetivos_Descartados = new ArrayList<Node>();
	public Node Objetivo_Valido, ObjetivoGusano = null,Yo;
	
	public Arbol arb;
	public frame fr;
	
	public ArrayList<Integer> obstaculos;
	public boolean noSol = false, enemi = false;
	
	private Start astart;
	
	
	public Node run(StateObservation observacion) {
		this.astart = new Start();
		this.observacion = observacion;
		this.vision();
		//this.fr = new frame(observacion.getWorldDimension().width, observacion.getWorldDimension().height);
		//this.fr.update(this.celda, this.observacion.getAvatarPosition());
		return this.decision();
		
				
		
	}
	
	public Mapa(ArrayList<Integer> obstaculos2, StateObservation ob) {
		this.astart = new Start();
		obstaculos = obstaculos2;
		//this.fr = new frame(ob.getWorldDimension().width, ob.getWorldDimension().height);
		
	}
	
	
	public void vision() {
		this.Obs_inamobibles  = this.observacion.getImmovablePositions();
		this.ENEMIGOS.clear();this.GUSANOS.clear();this.OBJETIVOS.clear();
		
		this.celda = this.observacion.getObservationGrid();
		this.Yo = new Node(observacion.getAvatarPosition());
		
		Conversion(this.Yo);
		
		this.enemi = false;
		//System.out.println(Arrays.deepToString(observacion.getImmovablePositions()));
		
		coste = new int[celda.length][celda[0].length];
		
		gen_Enemigos();
		gen_Objetivos();
		gen_Gusanos();
		
		updateEnemi();
		
	}
	
	
	public void gen_Enemigos() {
		for (ArrayList<Observation> ene_list : this.observacion.getNPCPositions(this.observacion.getAvatarPosition())) {
			for (Observation ene : ene_list) {
				if(ene.itype == 5) this.ENEMIGOS.add(ene);
			}
		}
	}
	
	public void gen_Objetivos() {
		for (ArrayList<Observation> ob_list : observacion.getNPCPositions(this.observacion.getAvatarPosition())) {
			for (Observation ob : ob_list) {
				if(ob.itype == 6) this.OBJETIVOS.add(ob);
			}
		}
	}
	
	public void gen_Gusanos() {
		for (ArrayList<Observation> Lobservation : observacion.getImmovablePositions(this.observacion.getAvatarPosition())) { 
			for (Observation obs : Lobservation) { 
				if(obs.itype == 3) this.GUSANOS.add(obs);
			}
		}
	}
	
	
	
	/**
	 * Devuelve el la gabiota mas cercano segun el numero de pasos de que devuelve el A*.
	 * 
	 * */
	public Node CazarGabiota() {
		int minP = Integer.MAX_VALUE;
		Node aux = null, min =  null;
		
		for (Observation observation : this.OBJETIVOS) {
			Node n = new Node(observation.position);
			
			Conversion(n);
			
			aux = this.minStart(this.Yo, n);
			int np = this.astart.npasos;
			
			if(minP > np) {
				minP = np;
				min = aux;
			}
			
		}
		
		return min;
	}
	
	private boolean GusanoPeligro() {
		int [] xl = { 1,2,3, -1,-2,-3};
    	int [] yl = { 1,2,3, -1,-2,-3};
    	
    	for (Observation ene : this.GUSANOS) {
    		 int px = (int)ene.position.x  / observacion.getBlockSize();
		     int py = (int)ene.position.y / observacion.getBlockSize();
		     
		     for(int x = 0; x  < xl.length; x++) {
		    	 for(int y = 0; y < yl.length; y++) {
		    		 if((px + xl[x]) > 0 && (px + xl[x]) < celda.length && (py + yl[y]) > 0 && (py + yl[y]) < celda[(px + xl[x])].length) { 
		    			 ArrayList<Observation> ListaO = this.celda[px + xl[x]][py + yl[y]];
		    			 for (Observation ob : ListaO) {
							if(ob.itype == 6 ) {
								Node ObjetivoNOde = new Node(ob.position);
								Conversion(ObjetivoNOde);
								this.ObjetivoGusano = ObjetivoNOde;
								return true;
							}
						}
		    		 }
		    	 }
		     }
		}
    	
    	return false;
	}
	
	private void Conversion(Node n) {
		n.p_actual.x = n.p_actual.x / observacion.getBlockSize();
		n.p_actual.y = n.p_actual.y / observacion.getBlockSize();
	}
	
	public Node decision() {
		arb = new Arbol();
		
		if(observacion.getAvatarPosition() != null) {
		while(arb.raiz instanceof pregunta) {
			Arbol.pregunta pr = (pregunta) arb.raiz;
			
			switch (pr.pregunta) {
			
			case Ac_Pr.PR_GUSANOS:
				if(this.GusanoPeligro())
					arb.raiz = arb.raiz.verdad;
				else 
					arb.raiz = arb.raiz.falso;
				break;
			default:
				break;
			}
		}
			
		if(arb.raiz instanceof Accion) {
			Arbol.Accion ac = (Accion) arb.raiz;
			
			switch (ac.accion) {
			case Ac_Pr.AC_CAZAR:
				//System.out.println("AC_CAZAR");
				return cazar();				
				
			case Ac_Pr.AC_CAZAR_OBJETIVO:
				//System.out.println("AC_CAZAR_OB");
				return cazarG();
				
			default:
				return null;
				
			}
		}
		}
		return null;
	}
	
	
	private Node cazarG() {
		
		//System.out.println(this.ObjetivoGusano);
		//System.out.println(this.Yo);
		return this.astart.run(this.Yo, this.ObjetivoGusano, this);
	}
	

	private Node cazar() {
	    return this.CazarGabiota();
	   // return  this.astart.run(this.Yo, this.Objetivo_Valido, this);
	}




	
    private boolean isObstacle(Vector2d newVector){
    	
        if(newVector.x < 0 || newVector.x >= celda.length) return true;
        if(newVector.y < 0 || newVector.y >= celda[0].length) return true;
        
        for(Observation obs : celda[(int) newVector.x ][(int) newVector.y])
        {
        	
        	if(obs.category == 3 && obs.itype == 5) { this.enemi = true; return true;}
            if(obs.category == 4 && obs.itype == 3) return false;
            if(obstaculos.contains(obs.category))   return true;
            
            
            
        }
        
        return false;

    }
    
    public ArrayList<Node> GeneradorDescendiente(Node node, Node destino) {
        ArrayList<Node> hijos = new ArrayList<Node>();
        int x = (int) node.p_actual.x; 
        int y = (int) node.p_actual.y; 

        for(int i = 0; i < x_arrNeig.length; ++i){
        	Vector2d newVector = new Vector2d( x_arrNeig[i] + x,  y_arrNeig[i] + y);
            if(!isObstacle(newVector)){
            	Node hijo = new Node(newVector);
            	hijo.action = node.setMoveDir(hijo);
            	hijo.coste_castigo = this.coste[x_arrNeig[i] + x][y_arrNeig[i] + y];
            	hijos.add(hijo);
            }
        }

        return hijos;
    }
    
    public static double heuristicEstimatedCost(Node curNode, Node goalNode){
        
        double xDiff =  Math.pow(Math.abs(curNode.p_actual.x - goalNode.p_actual.x),2);
        double yDiff =  Math.pow(Math.abs(curNode.p_actual.y - goalNode.p_actual.y),2);
        return Math.sqrt(xDiff + yDiff);
    }
    
    public Node  minStart(Node origin, Node Destino) {
    	Node res = this.astart.run(origin, Destino, this);
    	return res;
    }
    
    public void updateEnemi() {
    	int [] xl = { 1,2,3,4,5,6, -1,-2,-3,-4,-5,-6};
    	int [] yl = { 1,2,3,4,5,6, -1,-2,-3,-4,-5,-6};
    	
    	for (Observation ene : this.ENEMIGOS) {
    		 int px = (int)ene.position.x  / observacion.getBlockSize();
		     int py = (int)ene.position.y / observacion.getBlockSize();
		     
		     for(int x = 0; x  < xl.length; x++) {
		    	 for(int y = 0; y < yl.length; y++) {
		    		 if((px + xl[x]) > 0 && (px + xl[x]) < celda.length && (py + yl[y]) > 0 && (py + yl[y]) < celda[(px + xl[x])].length) { 
		    			 coste[px + xl[x]][py + yl[y]] = (6 - Math.abs(xl[x])) + (6 - Math.abs(yl[y])) * 2;
		    		 }
		    	 }
		     }
		}
    	
    }
    
   
}


