package Zunderene_02;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;
import javax.swing.JPanel;

import core.game.Observation;
import tools.Vector2d;

public class frame extends JFrame {
	public int i = 0;
	public ArrayList<Observation>[][] grid;
	public Vector2d avatar;
	
	public frame(int mx, int my) {
		super("interfaz");
		this.setSize(mx, my);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setBackground(Color.red);
	}
	
	public void update(ArrayList<Observation>[][] grid, Vector2d avatar) {
		this.removeAll();
		this.grid = grid;
		this.avatar = avatar;
		this.repaint();
	}
	
	public void paint(Graphics g) {
		g.clearRect(0, 0, this.getWidth(), this.getHeight());
		for(int x = 0; x < this.grid.length; x++) {
			for(int y = 0; y < this.grid[x].length; y++ ) {
				for(int z = 0; z < this.grid[x][y].size(); z++) {
					//System.out.println(Arrays.deepToString(grid));
					if(this.grid[x][y].get(0).category ==4) {g.setColor(Color.green);}
					if(this.grid[x][y].get(0).itype == 5) {g.setColor(Color.black);}
					if(this.grid[x][y].get(0).itype == 3) {g.setColor(Color.pink);}
					if(this.grid[x][y].get(0).itype == 6) {g.setColor(Color.white);}
					//if(this.avatar.x == x*28 && this.avatar.y == y*28) { g.setColor(Color.blue);}
					g.fillRect((int)this.grid[x][y].get(0).position.x , (int)this.grid[x][y].get(0).position.y, 28, 28);
					
				}
			}
		}
		g.setColor(Color.blue);
		g.fillRect((int)avatar.x , (int)avatar.y, 28, 28);
		
		
		
		
	}
	
	
}
