package Zunderene_02;

import Zunderene_02.Arbol.Accion;
import Zunderene_02.Arbol.pregunta;

public class Arbol {
	public Nodo raiz;
	
	
	
	public class Nodo {
		Nodo respuesta;
		Nodo verdad, falso;
		
		public Nodo(Nodo verdad, Nodo falso) {
			this.falso = falso;
			this.verdad = verdad;
		}
		
		public Nodo() {
			this.falso = null;
			this.verdad = null;
		}
		
	}
	
	 public class Accion  extends Nodo {
		public int accion;
		
		public Accion(int accion, Nodo verdad, Nodo falso) {
			super(verdad, falso);
			this.accion = accion;
		}
		
		public int getAccion() {
			return accion;
		}

		
	}
	
	public class pregunta extends Nodo {
		public int pregunta;
		
		public pregunta(int pregunta, Nodo verdadero,Nodo falso) {
			super(verdadero,falso);
			this.pregunta = pregunta;
		}
		
		
	}
	
	public Arbol() {
		this.raiz = (new pregunta(Ac_Pr.PR_GUSANOS, 
						new Accion(Ac_Pr.AC_CAZAR_OBJETIVO, null, null),
						new Accion(Ac_Pr.AC_CAZAR, null, null)));
	}
	
	
}
