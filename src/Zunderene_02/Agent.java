package Zunderene_02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Random;

import core.game.Observation;
import core.game.StateObservation;
import core.player.AbstractPlayer;
import ontology.Types;
import ontology.Types.ACTIONS;
import tools.Direction;
import tools.ElapsedCpuTimer;
import tools.Vector2d;
import tracks.singlePlayer.advanced.sampleMCTS.SingleMCTSPlayer;

public class Agent extends AbstractPlayer {

    public int num_actions;
    public ArrayList<ACTIONS> actions;
    public Mapa map;
    public Node under;
    
    /*
     * Categoria para los amovibles es 6.
     * * Type 8 hacia la derecha lento (Posicion +x esta ocupada)
     * * Type 11 haceia izquierda lento. (Posicion -x esta ocupada)
     * * Type 10 hacia la derecha rapido (Posicion ++x)
     * * Type 7 hacia la izquierda rapido
     * */
    public Agent(StateObservation so, ElapsedCpuTimer elapsedTimer){
    	// Lista de objetos inamovibles.
    	ArrayList<Integer> Obstaculos = new ArrayList<>();
    	
    	//Obstaculos.add(0);
    	//Obstaculos.add(2);
    	Obstaculos.add(4);
    	Obstaculos.add(6);

    	
    	
    	map = new Mapa(Obstaculos,so);
    }


    public Types.ACTIONS act(StateObservation stateObs, ElapsedCpuTimer elapsedTimer) {
    	
		Node m = map.run(stateObs);
		
		//System.out.println(Arrays.deepToString(stateObs.getMovablePositions()));
		
		if(m != null) {
			Types.ACTIONS action = Types.ACTIONS.fromVector(m.action);
    		//System.out.println(action);
    		under = m;
    		//stateObs.advance(action);
    		return action;
		}
		else if(under != null && map.noSol) {
			Direction a = under.setMoveDir(map.Yo);
			Types.ACTIONS action = Types.ACTIONS.fromVector(m.action);
			System.out.println("No solucion" + action);
			//stateObs.advance(action);
			return action;
		}
		else {
			System.out.println("No solucion");
			return Types.ACTIONS.ACTION_NIL;
		}
    	
    	//System.out.println(Arrays.deepToString(stateObs.getNPCPositions()));
    	
    	//return Types.ACTIONS.ACTION_NIL;
    		
    	
    	
    }
}