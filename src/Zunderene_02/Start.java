package Zunderene_02;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;





public class Start {
	private PriorityQueue<Node> L_Abiertos, L_Cerrados; 
	private Node origen,destino;
	public HashMap<Integer, ArrayList<Node>> pathCache;
	public int npasos = 0;
	
	
	public Start() {
		 pathCache = new HashMap<Integer, ArrayList<Node>>();
	}
	
	public Node run(Node origen, Node destino, Mapa map) {
		Node pivote;
		this.npasos = 0;
		L_Abiertos = new PriorityQueue<Node>();
		L_Cerrados = new PriorityQueue<Node>();
		
		this.origen = origen;
		this.destino = destino;
		
		this.origen.coste_totales = 0;
		this.destino.coste_totales = Mapa.heuristicEstimatedCost(origen, destino);
		
		L_Abiertos.add(origen);
		
		do {
			pivote = L_Abiertos.poll();
			L_Cerrados.add(pivote);
			
			ArrayList<Node> descendencia = map.GeneradorDescendiente(pivote, destino);
			
			for(int index = 0; index < descendencia.size(); index++) {
				Node descendiente = descendencia.get(index);
				double cost_distance = descendiente.coste_totales;
				
				
				
				if(!L_Abiertos.contains(descendiente) && !L_Cerrados.contains(descendiente)) {
					descendiente.coste_totales = (cost_distance + pivote.coste_totales);
					descendiente.coste_estimado = Mapa.heuristicEstimatedCost(descendiente, destino) * (descendiente.coste_castigo + 1);
					descendiente.progenitor = pivote;
					L_Abiertos.add(descendiente);
				}
				else if(cost_distance + pivote.coste_totales < descendiente.coste_totales) {
					descendiente.coste_totales = (cost_distance + pivote.coste_totales) * (descendiente.coste_castigo + 1);
					descendiente.progenitor = pivote;
					
					if(L_Abiertos.contains(descendiente))
						L_Abiertos.remove(descendiente);
					
					if(L_Cerrados.contains(descendiente))
						L_Cerrados.remove(descendiente);
					
					L_Abiertos.add(descendiente);
				}
			}
			
			
		}while(!L_Abiertos.isEmpty() && !pivote.p_actual.equals(this.destino.p_actual));
		
		if(!pivote.p_actual.equals(destino.p_actual)) {
			this.npasos = Integer.MAX_VALUE;
			return null;
		}
		
		if(pivote.progenitor != null) {
			while(pivote.progenitor.progenitor !=null) {
				pivote = pivote.progenitor;
				this.npasos++;
			}
		}
		
		else {
			return pivote;
		}
		
		return pivote;
		
	}
}
	
	