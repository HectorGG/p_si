package Zunderene_02;


/**
 * Codifica las acciones y las preguntas del arbol de toma de decisiones.
 * 
 * AC_CAZAR   		: BUSCA Y DA CAZA A LAS GABIOTAS BLANCAS LA MAS CERCANA.
 * AC_CAZAR_OTHER   : BUSCA OTRO OBJETIVO MENOS EL DESCARTADO.
 * AC_OBJETIVO 		: IRA AL ATAQUE DE LA GABIOTAL MAS CERCA DEL GUSANO.E
 *  
 * 
 * @author Hector Gonzalez Guerreiro.
 * 
 *
 */
public interface Ac_Pr {
	public static final int AC_CAZAR = 0;			
	public static final int AC_CAZAR_OTHER = 1;
	public static final int AC_CAZAR_OBJETIVO = 2;
	
	public static final int PR_ENEMIGOS = 3;
	public static final int PR_GUSANOS = 4;
	
}
